package com.example.myapplication

open class Calculator() {

    fun addValue(numOne: Double, numTwo: Double): Double {
        return numOne + numTwo
    }

    fun minusValue(numOne: Double, numTwo: Double): Double {
        return numOne - numTwo
    }

    fun deleteValue(numOne: Double, numTwo: Double): Double {
        return numOne / numTwo
    }

    fun multValue(numOne: Double, numTwo: Double): Double {
        return numOne * numTwo
    }

    private fun getAverage(vararg values: Double): Pair<Double, Double>? {
        if (values.elementAtOrNull(0) == null || values.elementAtOrNull(1) == null) {
            return null
        } else {
            return values[0] to values[1]
        }
    }

//    fun getAverage(vararg values: Double) =
//        if (values.elementAtOrNull(0) == null || values.elementAtOrNull(1) == null) null
//        else values[0] to values[1]

    private fun test() {
        val listOne = arrayOf(1, 2, 5, 4, 3, 5)
        val listTwo = arrayOf<Int?>(null, null, null, null)
        val listThree = arrayOf<Int>()

        listOne[0]  // 1
        listTwo[0] // null
        listThree[0] // упадет приложение
        listThree.elementAtOrNull(0) // null
    }
    }


