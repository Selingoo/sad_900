package com.example.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class SecondActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screen_two)

    }
}

class Taxi{
    fun main() {
        val asd = 9
        newAction(asd, false) // Методу не важно название переменной, методу важно какой у неё тип

        val a3:Unit
        val a4:Unit? = null

        val a1: (Int, Boolean) -> Unit = newAction(5, false)
        val a2:Unit = newAction2(10,false)
        foo(newAction(5,false))
        foo(newAction2)
        val list = listOf(5,515,15515)
        val newList = mutableListOf<Int>()
        list.forEach{ a: Int->
            if (a>100) {
                newList.add(a)
            }
            else if (a<10000){newList.add(a)}
            else return null
        }


        // параметр forEach - функция высшего порядка, возвращает только Unit , lkM + CNTR перекидывать на то где


    }
    fun foo(action:(a: Int, b: Boolean)-> Unit ){
        action.invoke(10,true)
    }


    fun newAction(a:Int,b:Boolean): (Int, Boolean) -> Unit {}
    val newAction2 = { a:Int, b:Boolean->}
    val filtertList = { a:Int ->
    }
}



//class MaxMinValue {

//    val realValue:MutableList<Int> = mutableListOf(1, 3, 7, 8, 11, 15)
//
//    fun checkValue(): List<Int>?{
//        realValue.forEach { action: Int ->
//            if (action > 5) {
//                realValue.add(action)
//            } else if (action < 10) {
//                realValue.add(action)
//            } else return null
//        }
//        }
//    }

//    val normalValues:List<Int> = realValue я думаю что это строка не нужная в таком виде,была под realValue, думал что в неё перезаписать список, но думаю перезаписать можно т.к. mutableListOf изменяемый
//    я хочу не перезаписывать на этот этот лист normalValues, а изменить (перезаписать) realValue

// мысль про красное, что не нравится андроиду,      fun checkValue():List<Int>? - >
//моя логика таков . лист realValue проходит отбор в checkValue, что больше 5 добавляется(перезаписуетя) в realValye так-же и с 10-й(только что меньше но я явно не указал что вернуть, я добавил эти значение куда-то, но не вернул
//
class MaxMinValue {

    val realValue:MutableList<Int> = mutableListOf(1, 3, 7, 8, 11, 15)

    fun checkValueOne():MutableList<Int>{
        val list:MutableList<Int> = mutableListOf()
        realValue.forEach{itName:Int ->
            if (itName>5&&itName<10){
                list.add(itName)
            }
        }
        return list
    }
 fun checkValueTwo(): MutableList<Int> {
     return realValue.filter {it>5&&it<10}.toMutableList()
 }
}
//    fun checkValue(): Unit? {
//        return realValue.forEach { action: Int ->
//            if (action > 5) {
//                realValue.add(action)
//            } else if (action < 10) {
//                realValue.add(action)
//            } else return null
//        }
//        }
//    }
// не работает т.к. 1) не правильно поставил условие 2) метод forEach перебирает а не фильтрует из-за этого нужно было добавить в другой пустой лист

