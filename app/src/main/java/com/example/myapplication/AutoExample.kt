package com.example.myapplication

// 5) Каждый автомобиль имеет набор параметров (скорость, количество колес, название марки, цена)
abstract class Car() {abstract val markName: String
                      abstract val price: Double
                      abstract val speed: Int
                      var wheelsCount: Int = 4 }


data class TeslaCar(
        override val markName: String,
        override val price: Double,
        override val speed: Int) : Car()

data class LadaCar(
    override val markName: String,
    override val price: Double,
    override val speed: Int
) : Car()

data class LambaCar(
    override val markName: String,
    override val price: Double,
    override val speed: Int
) : Car()

data class Porsche(
        override val markName: String,
        override val price: Double,
        override val speed: Int
) : Car ()

class AutoBusiness {

    // 2) есть список марок автомобилей
    val listOfCar: List<Car> = listOf(
        LadaCar("calina", 200.0, 120),
        LadaCar("priora", 180.0, 80),
        LambaCar("RT200", 10000.0, 280),
        TeslaCar("Tx200W", 9999.0, 180),
        Porsche ("Panamero", 25000.0, 240 )
    )

    // 1) завод авто, у которого можно узнать есть ли лицензии на изготовление тесла, лада, ламборджини.
    val isHaveLicenseForTesla: Boolean = false
    val isHaveLicenseForLamba: Boolean = false
    val isHaveLicenseForLada: Boolean = false
    val isHaveLicenseForPorsche: Boolean = false

    fun main() {
        buyCarBy(mark = "calina")
        buyCarBy(minMax = 0.0 to 1000.0)

        val myCar = buyCustomCar(1_000_000.0, 400, 6)
    }

    fun AxeCar() {
      buyCarBy(mark="Pedro")
      buyCarBy(minMax = 228.0 to 322.0)

        val ramzes666 = buyCustomCar(0.8, 0 , 3)
    }

    // 3) есть заказ на изготовление автомобиля с каким то брендом, в таком ценовом диапазоне
    fun buyCarBy(mark: String? = null, minMax: Pair<Double, Double>? = null, needForSpeed:Int? = null): List<Car> {
        val newList: List<Car>

        if (mark != null) {
            newList = listOfCar.filter {
                it.markName == mark
            }
        } else if (minMax != null) {
            newList = listOfCar.filter { it.price in minMax.first..minMax.second }
        }

        else if (needForSpeed != null)
            newList = listOfCar.filter {
                it.speed>100
            }
        else if (minMax != null && needForSpeed !=null ) {
            newList = listOfCar.filter { it.price in minMax.first..minMax.second && it.speed>100 }
        }

        else newList = listOf()

        return newList.filter {
            if (it is LambaCar && isHaveLicenseForLamba) {
                true
            } else if (it is LadaCar && isHaveLicenseForLada) {
                true
            } else if (it is TeslaCar && isHaveLicenseForTesla) {
                true
            } else it is Porsche && isHaveLicenseForPorsche
        }
    }


    // 4) есть заказ на изготовление кастомного автомобиле
    fun buyCustomCar(price: Double, speed: Int, wheelCount: Int): Car {
        val customCar = object : Car() { // анонимный клас
            override val markName: String = ""
            override val price: Double = price
            override val speed: Int = speed
        }

        customCar.wheelsCount = wheelCount

        return customCar
    }


    fun main2(): List<Int> {
        val test = listOf<Int>(1, 5, 7, 2, 342)
        val newTest = test.filter { it in 1..6 }
        return newTest
    }

    fun main3(): List<Int> {
        val test = mutableListOf<Int>(1, 5, 7, 2, 342)
        for (i in test.size downTo 0 step 1) {
            if (i !in 1..6) {
                test.removeAt(i)
            }
        }
        return test
    }
}