package com.example.myapplication

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("activity", "onCreate")
        setContentView(R.layout.activity_main)
        val flowCalculator:Calculator=Calculator()

        val listener = findViewById<Button>(R.id.listener)
        val secondpage = findViewById<Button>(R.id.secondscreen)

        val onespace = findViewById<EditText>(R.id.fieldOne)
        val twospace = findViewById<EditText>(R.id.fieldTwo)
        val rg = findViewById<RadioGroup>(R.id.radioGroup)
        val result = findViewById<Button>(R.id.result)

        result.setOnClickListener {
            val intent = Intent(this@MainActivity, SecondActivity::class.java)
            startActivity(intent)
            startActivity(Intent(this@MainActivity, SecondActivity::class.java))

            if (onespace.text.toString().isNullOrBlank()) {
                Toast.makeText(applicationContext, "введите 1-е поле", Toast.LENGTH_SHORT).show()
            } else if (twospace.text.toString().isNullOrBlank()) {
                Toast.makeText(applicationContext, "введите 2-е поле", Toast.LENGTH_SHORT).show()
            } else {
                val num1 = onespace.text.toString().toDouble()
                val num2 = twospace.text.toString().toDouble()

                val value: Double = when (rg.checkedRadioButtonId) {
                    R.id.plus -> flowCalculator.addValue(num1,num2)
                    R.id.minus -> flowCalculator.minusValue(num1,num2)
                    R.id.multiply -> flowCalculator.multValue(num1,num2)
                    R.id.delete -> flowCalculator.deleteValue(num1,num2)
                    else -> 0.0
                }

                result.textSize = 25F
                result.setBackgroundColor(Color.RED)
                result.text = value.toString()
            }
        }
        onespace.doOnTextChanged { t, _, _, _ -> onespace.setBackgroundColor(Color.WHITE) }
    }

    override fun onStart() {
        super.onStart()
        Log.d("activity", "onStart")
    }

    override fun onPause() {
        super.onPause()
        Log.d("activity", "onPause")

    }

    override fun onResume() {
        super.onResume()
        Log.d("activity", "onResume")
    }

    override fun onStop() {
        super.onStop()
        Log.d("activity", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("activity", "onDestroy")
    }
}

class  UserCal () {
    val FlowCalcultator: Calculator = Calculator()


    private fun addValue(numOne:Double, numTwo:Double):Double {
        return numOne + numTwo
    }

        private fun minusValue(numOne: Double, numTwo: Double): Double {
            return numOne - numTwo
        }

        private fun deleteValue(numOne: Double, numTwo: Double): Double {
            return numOne / numTwo
        }

        private fun multValue(numOne: Double, numTwo: Double): Double {
            return numOne * numTwo
        }
    }


// val FlowCalcultator: Calculator = Calculator()
// val FlowCalcultator = Calculator()
// ку, у меня мысль, 2-я строчка не будет работать правильно, т.к. параметры(тип) мы не указали, правильно?